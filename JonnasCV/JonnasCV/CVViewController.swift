//
//  ViewController.swift
//  JonnasCV
//
//  Created by Jonna Lennartsson on 2018-12-06.
//  Copyright © 2018 Jonna Lennartsson. All rights reserved.
//

import UIKit

class CVViewController: UIViewController {

    @IBOutlet weak var profileImage: RoundImageView!
    @IBOutlet weak var contactInfoLabel: UILabel!
    @IBOutlet weak var presentationLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //profileImage.image = UIImage(named: "JAG")
        contactInfoLabel.text = "Jonna Lennartsson\n lennartsson.jonna@live.se\n 0760327968\n"
    }
    
    @IBAction func expClicked(_ sender: Any) {
                performSegue(withIdentifier: "firstSegue", sender: self)
    }

    @IBAction func skillsClicked(_ sender: Any) {
                performSegue(withIdentifier: "thirdSegue", sender: self)
    }
    
}

