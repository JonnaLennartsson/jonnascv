//
//  ExperienceDetailViewController.swift
//  JonnasCV
//
//  Created by Jonna Lennartsson on 2018-12-06.
//  Copyright © 2018 Jonna Lennartsson. All rights reserved.
//

import UIKit

class ExperienceDetailViewController: UIViewController {
    
    /*
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLableView: UILabel!
    @IBOutlet weak var yearLableView: UILabel!
    @IBOutlet weak var descriptionView: UILabel!
    */
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLableView: UILabel!
    @IBOutlet weak var yearLableView: UILabel!
    @IBOutlet weak var descriptionView: UILabel!
    
    var experience: Experience = Experience(experience: " ", year: " ", description: " ", imageName: " ")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageView.image = UIImage(named: experience.imageName)
        titleLableView.text = experience.experience
        yearLableView.text = experience.year
        descriptionView.text = experience.description
        
    }
}
