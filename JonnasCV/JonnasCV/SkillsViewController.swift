//
//  SkillsViewController.swift
//  JonnasCV
//
//  Created by Jonna Lennartsson on 2018-12-06.
//  Copyright © 2018 Jonna Lennartsson. All rights reserved.
//

import UIKit

class SkillsViewController: UIViewController {


    @IBOutlet weak var skillsImageView: UIImageView!
    
    var images: [UIImage] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        images = [
            UIImage(named: "Dino1"),
            UIImage(named: "Dino2"),
            UIImage(named: "Dino3"),
            UIImage(named: "Dino4"),
            UIImage(named: "Dino5"),
            UIImage(named: "Dino6"),
            UIImage(named: "Dino7"),
            UIImage(named: "Dino8"),
            UIImage(named: "Dino9"),
            UIImage(named: "Dino10"),
            UIImage(named: "Dino11"),
            UIImage(named: "Dino12"),
            UIImage(named: "Dino13"),
            UIImage(named: "Dino14"),
            UIImage(named: "Dino15"),
            UIImage(named: "Dino15"),
            UIImage(named: "Dino16"),
            UIImage(named: "Dino17"),
            UIImage(named: "Dino18"),
            UIImage(named: "Dino19"),
            UIImage(named: "Dino20"),
            UIImage(named: "Dino21"),
            UIImage(named: "Dino22"),
            UIImage(named: "Dino23"),
            UIImage(named: "Dino24"),
            ] as! [UIImage]
        
        skillsImageView.animationImages = images
        skillsImageView.animationDuration = 1.8
        skillsImageView.startAnimating()
    }

}
