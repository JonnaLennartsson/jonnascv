//
//  ExperienceViewController.swift
//  JonnasCV
//
//  Created by Jonna Lennartsson on 2018-12-06.
//  Copyright © 2018 Jonna Lennartsson. All rights reserved.
//

import UIKit

class ExperienceViewController: UIViewController {

    @IBOutlet var tableView: UITableView!
    
    var workExperience: [Experience] = []
    var education: [Experience] = []
    var experience: [[Experience]] = []
    var sectionTitle: [String] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        workExperience = [
            Experience(experience: "Ikea", year: "2015 - ", description:"Lagermedarbetare\n\nTruckförare, plockar och packar varor till kund.", imageName: "IKEA"),
            Experience(experience: "Elgiganten", year: "2014 - 2015", description: "Kundtjänst\n\nKundkontakt via telefon och mail, problemlösning inom logistik och service.", imageName: "elgiganten"),
            Experience(experience: "Arla Foods", year: "2014", description: "Lagermedarbetare\n\nSommarjobb som truckförare, plock och pack av varor till kund.", imageName: "arla"),
            Experience(experience: "Malmborgs Lilla", year: "2012 - 2013", description: "Barista\n\nServering, kassa, beställningar och övriga sysslor inom en caféverksamhet.", imageName: "malmborgs"),
            Experience(experience: "Malmborgs Konditori", year: "2007 - 2012", description: "Barista\n\nServering, kassa, beställningar och övriga sysslor inom en caféverksamhet.", imageName: "malmborgs"),]
        
        education = [
            Experience(experience: "Tekniska högskolan i Jönköping" , year: "2017 -", description: "Högskoleingenjörsexamen i Datateknik\n\ninriktning Mjukvaruutveckling och mobila plattformar,", imageName: "JU"),
            Experience(experience: "Tekniska högskolan i Jönköping", year: "2016 - 2017", description: "Tekniskt Basår\n\nKompleterande program av behörighet inom matematik, fysik och kemi." , imageName: "JU"),
            Experience(experience: "Värnamo folkhögskola", year: "2013 - 2014", description: "Textilakademin\n\nGrundutbildning inom skapande, sömnad och design.", imageName: "värnamoFH"),
            Experience(experience: "Per Brahegymnasiet", year: "2007 - 2010", description: "Estetiska Programmet - Musik\n\nGymnasialutbildning med inriktning på musik." , imageName: "perBrahe"),]
        
        experience = [workExperience, education]
        
        sectionTitle = ["Work Experience", "Education"]
        
        tableView.tableFooterView = UIView()
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? ExperienceDetailViewController {
            if let indexPath = sender as? IndexPath {
                let exp = experience[indexPath.section][indexPath.row]
                destination.experience = exp
            }
        }
    }
    
}

// MARK: -TableView delegates
extension ExperienceViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return experience[section].count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ExperienceTableViewCell", for: indexPath) as? ExperienceTableViewCell{
            let exp = experience[indexPath.section][indexPath.row]
            cell.expLabel.text = exp.experience
            cell.yearLabel.text = exp.year
            cell.expImageView.image = UIImage(named: exp.imageName)
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 30))
        headerView.backgroundColor = UIColor(named: "pinkish")
        let headerLabel = UILabel(frame: CGRect(x: 8, y: 0, width: headerView.frame.width - 16, height: 30))
        headerLabel.text = sectionTitle[section]
        headerLabel.textColor = UIColor.black
        headerLabel.font = UIFont.boldSystemFont(ofSize: 15)
        headerView.addSubview(headerLabel)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath)
        performSegue(withIdentifier: "secondSegue", sender: indexPath)
    }
    

}
