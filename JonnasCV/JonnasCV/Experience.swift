//
//  Experience.swift
//  JonnasCV
//
//  Created by Jonna Lennartsson on 2018-12-06.
//  Copyright © 2018 Jonna Lennartsson. All rights reserved.
//

import Foundation

struct Experience {
    var experience: String
    var year: String
    var description: String
    var imageName: String
}
