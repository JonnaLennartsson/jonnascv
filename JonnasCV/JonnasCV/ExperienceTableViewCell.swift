//
//  ExperienceTableViewCell.swift
//  JonnasCV
//
//  Created by Jonna Lennartsson on 2018-12-06.
//  Copyright © 2018 Jonna Lennartsson. All rights reserved.
//

import UIKit

class ExperienceTableViewCell: UITableViewCell {
 
    @IBOutlet weak var expLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var expImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
